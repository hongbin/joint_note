With LHC luminosities over $10^{34}$cm$^{-2}$s$^{-1}$ achieved starting in 2016 data-taking, a specific
problem appeared in the calorimeters, where fake jets were reconstructed in the endcaps
inner wheels. The fake jets always happened during the first bunches of a bunch-train.
Detailed investigations have shown that the original cause of this behaviour is a
negative saturation of the signal in the high gain shaper.
The issue has been cured starting with 2017 data-taking by configuring the electronics to never
readout the high gain, but to rely on medium and low gain only.

%It comes from the accumulation
%of all the negative lobes of all the pileup signals, that is not counter-acted by positive
%signal in the gaps between the bunch-trains. The issue was limited to the first layer of
%the EMEC inner wheels, where the pileup contribution is the largest due to the wide size
%of the front cells.
Simulations have allowed to understand the origin of this negative saturation. Basically,
its origin lies in that the analogue electronics has been designed and carefully optimised
to work on unipolar signals. However, at high luminosities the accumulation of pileup
signals over many bunch-crossings creates an average minimum bias current in the chain
that can be significantly different from 0. The effect is particularly important in the
first layer of the EMEC inner wheels, where the pileup contribution is the largest due to
the wide size of the front cells in this quite forward region of the calorimeter. With respect to such a positive average current, the LAr
signals over an LHC orbit can be considered bipolar: in particular, the absence of current in the gaps
in between the bunch trains is seen as a negative signal from the LAr electronics. Since
the LAr electronics has been designed for unipolar signals, the margins for signals of
wrong polarity are small, hence the negative saturation in the gaps. It then takes several
bunch-crossings for the electronics to recover a correct behavior at the beginning of a
train.

Given that the pileup levels will be several times larger at the HL-LHC, it is worth
investigating the behavior of the future front-end electronics to such a behavior
implying signal over full orbits of the LHC. More precisely, it is important to assess
whether the prototypes are immune to such effects, and if not whether simple design
decisions can be made to overcome them.

To study this effect, a typical signal has been programmed on an arbitrary waveform
generator for a full LHC orbit, including a realistic LHC bunch structure.  The signal
mimics the effect of an average pileup with a given DC amplitude during the bunch-trains,
with a small sinusoidal signal of 5~MHz frequency superimposed to understand the response
of the chip to transient signals under such a pileup load. The signal was
then input to LAUROC0 50B and 25A channels at several "pileup" amplitudes.

Figure~\ref{fig:LAUROC0_fullOrbit_HG} presents the measured preamplifier output of the
high gain for the
channel 50B in such a setup. The signal was injected over a 12~k$\Omega$ resistor, so that
a pileup amplitude of 400~mV peak-to-peak corresponds to an input current of 33~$\mu$A.
The signal is inverted, so that the region between $\sim3000$~ns and $\sim12000$~ns
corresponds to the abort gap. The figure shows that for amplitudes starting about 400~mV,
the signal exhibits a negative saturation in the gaps between bunch-trains. The saturation
lasts as long as the gaps, so that the system will need several bunch-crossings at the
beginning of a train before
recovering a correct behavior. This could have consequences very similar to the ones
observed in Run~2 collision data. A second observation is that for high amplitudes the low
frequency component has a large amplitude. This could cause a problematic behavior in the
shaper.

\begin{figure}[htpb]
\begin{subfloat}
  \centering
  \includegraphics[width=.5\textwidth]{fullOrbit_LAUROC0_HG_full.pdf}
\end{subfloat}%
\begin{subfloat}
  \centering
  \includegraphics[width=.5\textwidth]{fullOrbit_LAUROC0_HG_detail.pdf}
\end{subfloat}%
\caption{Preamplifier output for full LHC orbits with a simulation of high pileup input,
at different amplitudes, for the high gain of the 50B channel. Left: output over the full orbit. Right: detail of the first 
bunch-train after the abort gap.}
\label{fig:LAUROC0_fullOrbit_HG}
\end{figure}

Figure~\ref{fig:LAUROC0_fullOrbit_LG} presents the output of the low gain of the 50B
channel in the same conditions. It shows that the low gain output is not completely immune
to this high pileup behaviour as well. The cause is the use of the internal discriminator
in LAUROC0, that can induce such an effect in the low gain output. More specifically, the
baseline shift induced inside the chip by the minimum bias inputs changes the actual
threshold for the discriminator, as function of the luminosity and of the bunch-filling
scheme.

\begin{figure}[htpb]
\begin{subfloat}
  \centering
  \includegraphics[width=.5\textwidth]{fullOrbit_LAUROC0_LG_full.pdf}
\end{subfloat}%
\begin{subfloat}
  \centering
  \includegraphics[width=.5\textwidth]{fullOrbit_LAUROC0_LG_detail.pdf}
\end{subfloat}%
\caption{Preamplifier output for full LHC orbits with a simulation of high pileup input,
at different amplitudes, for the low gain of the 50B channel. Left: output over the full orbit. Right: detail of the first 
bunch-train after the abort gap.}
\label{fig:LAUROC0_fullOrbit_LG}
\end{figure}

A similar test has been conducted on the 25B channel, and similar effects have been
observed starting at input currents above 0.4\~mA.

The negative saturation effect is linked to the "DC" input current being high enough to
depolarize the input transistor of LAUROC0. Designing the ASIC with a higher polarization
current could therefore reduce the effect, at the expense of a slightly larger parallel
noise.
%Part of the saturation effect is ... explication Christophe delpolarisation qui serait
% curable est-elle complete ?

This study shows that the LAUROC0 is not immune to negative saturation effects alike those
observed in the existing electronics. However, given the relatively large minimum bias
currents needed to trigger these effects, it is not clear yet if they can be triggered in
actual HL-LHC running conditions. The minimum bias input current going through the
preamplifiers in the large cells of the EMEC inner wheel in Run~2 was about 90~$\mu$A. It
is therefore plausible that the 0.4~mA current needed to trigger the effect in LAUROC0
will not be reached in HL-LHC. For the 50$\Omega$ channels, where the effect starts at
about 33~$\mu$A, studies will be needed to estimate the pileup input current expected
in the strips cells of the EMEC outer wheel ($2.0<\abseta<2.5$).

The effects of such large minimum bias average currents will anyway have to be re-assessed
with future prototypes including the CR(RC)2 shaper. Therefor it is important to estimate
the expected average currents up to $7.5 10^{34}$cm$^{-2}$s$^{-1}$. This can be achieved
by using the measured currents drawn by the high voltage power supplies in Run~2, that can
be transformed into a current per cell then scaled to a given luminosity.

